package com.sun.webmanage.pojo;

import com.sun.webmanage.model.TbItemParam;

/**
 * 规格参数拓展类
 * @author 孙哈哈
 *
 */
public class TbItemParamCustom extends TbItemParam{

	private String itemCatName;

	public String getItemCatName() {
		return itemCatName;
	}

	public void setItemCatName(String itemCatName) {
		this.itemCatName = itemCatName;
	}
	
	
}
