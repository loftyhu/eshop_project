<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312"/>
    <title>ESHOP-欢迎登录</title>
    <link type="text/css" rel="stylesheet" href="/css/login.css"/>
    <link href="/css/ego.css" rel="stylesheet"/>
    <script type="text/javascript" src="/js/global_url.js" charset="utf-8"></script>
    <script type="text/javascript" src="/js/jquery-1.6.4.js"></script>
    <link href="/images/passport.png" rel="shortcut icon"/>
    <style>
    #entry .mc {border: 1px solid #e1e1e1;background: #ffccc1;height: 475px;}
    #logo b {position: absolute;background: url(../images/welcome.png) 0 -131px;width: 129px;height: 40px;top: 18px;left: 267px;}
    #logo {padding:10px 0;float:none;margin-top:0;position:relative;position:relative;top:27px;left:-120px}
    
    #content .tips-wrapper{background:#fff8f0;width:100%;padding-top:10px;padding-bottom:10px;text-align:center}
    #content .tips-wrapper .cont-wrapper{display:inline-block;*display:inline;width:90%}
    #content .tips-wrapper .icon-tips{background:url(//misc.360buyimg.com/user/passport/1.0.0/widget/login-form-2018-0827/i/icon-tips.png);display:inline-block;width:16px;height:16px;vertical-align:middle;margin-right:5px}
    #content .tips-wrapper .tips-inner p{vertical-align:middle;color:#999;font-size:12px;display:inline-block;*display:inline}
    
    #entry .form{position:absolute;top:77px;right:348px;width:308px;overflow:hidden;height:310px;padding:39px 20px;align-content:center;background-color:white}
    #entry .item{height:80px;line-height:26px;overflow:visible;position:relative;width:100%;z-index:1}
    #entry .item .item-ifo{position:relative;width:100%}
    #entry .text{float:none;width:92%;height:24px;line-height:24px;padding:4px 20px 4px 3px;border:1px solid #ccc;font-size:14px;font-family:arial,"宋体"}
    #entry .coagent{position:absolute;right:0;width:250px;line-height:26px;z-index:99999;bottom: 50px;left: 52px;}
    
    .qr-coagent{background-color:#fff;font-family:"microsoft yahei";color:#999;border:0;padding:0;line-height:25px;}
    .coagent ul { display: block;}
    .qr-coagent li{text-align:left;position:relative;margin-right:18px;float:left;}
    .qr-coagent li b{width:25px;height:25px;display:block;background:url(//misc.360buyimg.com/user/passport/1.0.0/widget/login-form-2018-0827/i/qr-coagent.png) no-repeat;margin:0 auto;position:absolute;left:0}
    .qr-coagent li em{padding-left:32px;*display:inline-block;_display:inline}
    .qr-coagent li .faster{background-position:-27px}
    .qr-coagent li .more-safe{background-position:-54px}
    </style>
</head>
<body>
<div class="w">
    <div id="logo">
    	<a href="javascript:window.location.href=GLOBALURL.portal_base_address;" clstag="passport|keycount|login|01">
    		<img src="/images/ego-logo006.png" alt="易购" width="270" height="60"/>
    	</a><b></b>
   	</div>
</div>
<form id="formlogin" method="post" onsubmit="return false;">
<div id="content">
<div class=" w1" id="entry">
<div class="tips-wrapper"><div class="tips-inner"><div class="cont-wrapper"><i class="icon-tips"></i><p>依据《网络安全法》，为保障您的账户安全和正常使用，请尽快完成手机号验证！ 新版<a href="https://about.jd.com/privacy/" class="black" target="_blank">《网络安全隐私政策》</a>已上线，将更有利于保护您的个人隐私。</p></div></div></div>
        <div class="mc " id="bgDiv">
            <div id="entry-bg" clstag="passport|keycount|login|02" style="margin:auto;width: 990px; height: 100%;  background: url(/images/background.jpg) 0px 0px no-repeat;background-color: #ffccc1;">
			</div>
			
			<div class="form">
                <div class="item fore1">
                    <span>用户名</span>
                    <div class="item-ifo">
                        <input type="text" id="loginname" name="username" class="text"  tabindex="1" autocomplete="off"/>
                        <div class="i-name ico"></div>
                        <label id="loginname_succeed" class="blank invisible"></label>
                        <label id="loginname_error" class="hide"><b></b></label>
                    </div>
                </div>
                <script type="text/javascript">
                    setTimeout(function () {
                        if (!$("#loginname").val()) {
                            $("#loginname").get(0).focus();
                        }
                    }, 0);
                </script>
                <div id="capslock"><i></i><s></s>键盘大写锁定已打开，请注意大小写</div>
                <div class="item fore2">
                    <span>密码</span>
                    <div class="item-ifo">
                        <input type="password" id="nloginpwd" name="password" class="text" tabindex="2" autocomplete="off"/>
                        <div class="i-pass ico"></div>
                        <label id="loginpwd_succeed" class="blank invisible"></label>
                        <label id="loginpwd_error" class="hide"></label>
                    </div>
                </div>
                <div style="text-align: right;height: 47px;"><a href="javascript:void(0);">忘记密码？</a></div>
                <div class="item login-btn2013">
                    <input type="button" class="btn-img btn-entry" id="loginsubmit" value="登录" tabindex="8" clstag="passport|keycount|login|06"/>
                </div>
                <div class="coagent qr-coagent" id="qrCoagent">
                            <ul>
                                <li><b></b><em>免输入</em></li>
                                <li><b class="faster"></b><em>更快&nbsp;</em></li>
                                <li><b class="more-safe"></b><em>更安全</em></li>
                            </ul>
                        </div>
            </div>
            
        </div>
        <div class="free-regist">
            <span><a href="/user/showRegister" clstag="passport|keycount|login|08">免费注册&gt;&gt;</a></span>
        </div>
    </div>
    </div>
</form>
<!-- footer start -->
<jsp:include page="../commons/footer-links.jsp" />
<!-- footer end -->
<script type="text/javascript">
	var redirectUrl = "${redirect}";
	var LOGIN = {
			checkInput:function() {
				if ($("#loginname").val() == "") {
					alert("用户名不能为空");
					$("#loginname").focus();
					return false;
				}
				if ($("#nloginpwd").val() == "") {
					alert("密码不能为空");
					$("#nloginpwd").focus();
					return false;
				}
				return true;
			},
			doLogin:function() {
				$.post("/user/login", $("#formlogin").serialize(),function(data){
					if (data.status == 200) {
						alert("登录成功！");
						if (redirectUrl == "") {
							location.href = GLOBALURL.portal_base_address;
						} else {
							location.href = redirectUrl;
						}
					} else {
						alert("登录失败，原因是：" + data.msg);
						$("#loginname").select();
					}
				});
			},
			login:function() {
				if (this.checkInput()) {
					this.doLogin();
				}
			}
		
	};
	$(function(){
		$("#loginsubmit").click(function(){
			LOGIN.login();
		});
	});
</script>
</body>
</html>